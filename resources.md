---
title: "Resources"
layout: list_items_with_modals
---

## Materials

- [FLUSP presentation](/materials/Apresentacao_do_FLUSP.pdf) on the first
holliday's meeting (PT-BR)
- [First steps on Kernel](/materials/Kernel_Primeiros_Passos.pdf) (PT-BR)
- [First steps on GCC](/materials/GCC_Primeiros_Passos.pdf) (PT-BR)

## Hardware

<!-- Here comes our hardware itens -->
