---
layout: post
categories: [events, feed-english]
title: "KUnit Hackathon"
lang: en
ref: kunitHackathon2021Page
redirect_from: /events/2021/07/13/kunit_hackathon
author: marcelosc
excerpt_separator: <!--end-abstract-->
---

In partnership with [LKCAMP](https://lkcamp.dev), we organized a hackathon to
encourage new Linux kernel contributors.

<!--end-abstract-->

On 10th, July we at FLUSP joined with LKCAMP to provide a KUnit Hackathon to
kernel practitioners and newcomers. For us at FLUSP, it was a joy to partner
with folks from LKCAMP since both student groups are glad to share free software
development knowledge and experience with fellow kernel developers. For this
occasion, we introduced attendees to the KUnit framework.

### What happened at KUnit Hackathon

One week before the hackathon event, we organized a ramp-up day in which
we focused on providing background to kernel newbies. The pre-event day included
an introduction to the Linux kernel and its development process, along with an
introduction to KUnit. Besides our presentations on these topics, we also walked
through a [KUnit tutorial](https://www.kernel.org/doc/html/latest/dev-tools/kunit/start.html#writing-your-first-test)
in the morning. In the afternoon, we discussed patch creation and went
through the setup of git in preparation for forthcoming contributions.
Lastly, we gave some tips and guidelines on how to compile and runtest the Linux
kernel.

On the hackathon day, we started at 9 o'clock with a few slides presenting our
student groups, our schedule for the day, and how we would collaborate
throughout the event. As well as in past events, we encouraged people to make up
pairs or teams as this would likely increase knowledge exchange and possibly
make them more productive according to agile development philosophy. With this
regard, to provide a means for the attendees to speak to the coaches (and to
each other) without disturbing other teams was a great concern for us.
Nevertheless, we were able to overcome this problem by making use of a
BigBlueButton videoconference room along with a pool of Jitsi rooms for the
teams.

After the initial guidelines for the event, we once again went through the KUnit
tutorial (this time in a review fashion), then started working on selected
issues for this hackathon. For this gathering, we aimed to convert existing
runtime tests under lib/, to KUnit standards. We had 4 teams comprising 9
developers working in enhancing test_hexdump, test_hash, test_ubsan, and
test_parman tests with KUnit macros. We also had 5 tireless staff members who
helped the teams tackle each test conversion throughout the day. The activity
was tracked with a kanban on a Riseup pad containing the developers' names,
their Jitsi room, test name, and work progress (WIP, review, or sent to kernel
mailing list).

As of the ramp-up day, we had a 1-hour break for lunch at noon.  At half-past
15, we made a brief break to acknowledge each team's progress on the issues they
chose to tackle. Most teams kept on coding until we finally have to close the
event at 17 o'clock. Unfortunately, the issues chosen by the teams took more
development effort than we thought, such that none of them could finish
converting the tests on Saturday. However, although we agreed that the work done
was not ready to go to the mailing lists, we do believe the developers will be
able to complete those improvements in a few more hours of coding. Thus, we
encouraged the attendees to continue their work on the changes and participate
in our weekly meetings. Overall, it was a day of learning for both attendees and
staff developers.

Here are some prints I take during the event. Spotlight on a pirate version of
Mr.FLUSP :-]

{% include slideshow.html
    slideShowIndex = 0
    images = "kunit_hackathon1.png,kunit_hackathon2.png,kunit_hackathon3.png,kunit_hackathon4.png"
    caption_list="KUnit introduction,KUnit introduction 2,End of introduction. Let's code!,Our Kanban"
%}

### Slides

Here are the slides we used during the KUnit Hackathon.

<a href="https://gitlab.com/lkcamp/lkcamp_presentations_bin/-/blob/master/round3/lkcamp-M1.pdf">Introduction to the Linux kernel</a><br/>
<a href="https://gitlab.com/lkcamp/lkcamp_presentations_bin/-/blob/master/round3/lkcamp-M2.pdf">Linux Kernel Development Cycle</a><br/>
<a href="https://docs.google.com/presentation/d/1ZQrWxzogJRASmpmMkfnm0aNu9_ct-JOdPrQumq6iv2g/edit?usp=sharing">Introduction to KUnit (portuguese)</a><br/>

### Special thanks

We know that the KUnit Hackathon wouldn't have been possible without the help of
many community members. We would like to give a special thanks to the following
people:

* All FLUSP and LKCAMP members who helped with the event organization
* Brendan Higgins 
* Daniel Latypov
* Daniel Díaz

### Contact:

For more information, feel free to send us an e-mail:
[flusp@googlegroups.com](mailto:flusp@googlegroups.com)

Also, have a look at [LKCAMP's website](https://lkcamp.dev) for more content on
Linux kernel development.
